#-*- coding: Latin-1 -*-
""" Partie fonctions read et view"""
import os.path
from shutil import copy2
from phyml_util import FILE_EXTENTION_LIST

#prints the content of a file and returns it's content
def readhelper (file):
#     try:
    validerFichier(file)
    fichier = open (file, "r")
    readfile = ""
    for ligne in fichier:
#         ligne=ligne.rstrip("\n")
#         print (ligne)
        readfile+=ligne
    fichier.close()
#     except:
#     print("Le fichier n'existe pas.")
    return readfile
    

#returns a list of files created by phyml for an instance of runphyml
def view_helper (working_directory,original_input_filename,run_id,user_run_id_param):
    file_list=[ os.path.join(run_id, f) for f in os.listdir(working_directory) if os.path.isfile(os.path.join(working_directory, f)) and os.stat(os.path.join(working_directory, f)).st_size != 0]
    phyml_files = []
    ext = ".txt"
    if(user_run_id_param != ""): ext =  "_"+user_run_id_param+".txt"
    
    for f in file_list :
        for e in FILE_EXTENTION_LIST :
            if f.endswith(e+ext) and f != original_input_filename: phyml_files.append(f)
    return phyml_files


def contient_fichier_vide (FichList,working_directory):
    for file in FichList:
        if os.stat(os.path.join(working_directory, file)).st_size == 0:
            return True
    return False

def prep_working_directory(filepath,directorypath,process_id):
    #create subdirectory named process_id
    new_subdirectory = os.path.join(directorypath,process_id)
    if (not os.path.isdir(new_subdirectory)):
        os.makedirs(new_subdirectory)

    #copy file to new subdirectory
    f = copy2(filepath,str(new_subdirectory))
    return str(f)

def prep_working_directory_with_file(text,directorypath,process_id):
    #create subdirectory named process_id
    new_subdirectory = os.path.join(directorypath,process_id)
    if (not os.path.isdir(new_subdirectory)):
        os.makedirs(new_subdirectory)

    #copy file to new subdirectory
    with open(str(new_subdirectory,"test"), mode='w') as f:
        f.write(text)
    f.close()
    return f

def validerFichier(fichier):
    if os.stat(fichier).st_size == 0:
        raise Exception("Le fichier est vide.")

