__author__ = 'elodiehip-ki'
import sqlite3

ID_COLUMN = 0
# DATE_COLUMN = 1
FILENAME_COLUMN = 1
STATUS_COLUM = 2 
PARAM_COLUMN = 3

def init_db():
    conn = sqlite3.connect('phyml.db')
    query = ''' CREATE TABLE IF NOT EXISTS Phyml (
	        id 	  INTEGER NOT NULL,
		status  	  TEXT	NOT NULL,
		input         TEXT NOT NULL,
		parametres    TEXT	NOT NULL,
		PRIMARY KEY (id)
	    )'''
    conn.cursor().execute(query)

def add(record):
    try:
        conn = sqlite3.connect('phyml.db')
        cursor = conn.cursor()
        cursor.execute("INSERT INTO Phyml (status,input,parametres) VALUES (?,?,?)", record)
        conn.commit()
        return cursor.lastrowid
    except sqlite3.Error as e:
        print (e)

def delete(record):
    try:
        conn = sqlite3.connect('phyml.db')
        conn.cursor().execute("delete from Phyml where id=?", [record])
        conn.commit()
    except sqlite3.Error as e:
        print (e)

def setStatus(record):
    try:
        conn = sqlite3.connect('phyml.db')
        idstatus = conn.cursor().execute("update Phyml set status=? where id=?", record)
        conn.commit()
    except sqlite3.Error as e:
        print (e)

def setParams(record):
    try:
        conn = sqlite3.connect('phyml.db')
        idstatus = conn.cursor().execute("update Phyml set parametres=? where id=?", record)
        conn.commit()
    except sqlite3.Error as e:
        print (e)

def getStatus(record):
    try:
        conn = sqlite3.connect('phyml.db')
        idstatus = conn.cursor().execute("select status from Phyml where id=?", [record])
        return idstatus.fetchone()[0]
    except sqlite3.Error as e:
        print (e)
    return -1

def reset(record):
    try:
        conn = sqlite3.connect('phyml.db')
        conn.cursor().execute("delete from Phyml where id=? and status = 1 ", [record])
        conn.commit()
    except sqlite3.Error as e:
        print (e)

def get():
    records = []
    try:
        conn = sqlite3.connect('phyml.db')
        records = conn.cursor().execute("select * from Phyml")
    except sqlite3.Error as e:
        print (e)
    return records
