# -*- coding: utf-8 -*-
from phyml_util import PARAMS_VALIDES, PARAMS_OBLIGATOIRES, ALIAS_VALIDES,\
    PARAMS_APP
from read_and_viewfns_2 import validerFichier

def lireParam( fichier_param ):
    """ Retourne un dictionnaire des parametres lus dans le fichier passé en parametre """
    params = {}
    try:
        fh= open(fichier_param, "r")
        for ligne in fh:
            if not ligne.startswith('#'):
                valeurs = ligne.split(" = ")
                if len(valeurs) > 1 :
                    cle,valeur = valeurs
                    if valeur == "":
                        raise Exception("La ligne '",ligne,"' du fichier parametre n'est pas valide!")
                    params[cle] = valeur.rstrip("\n")
                else:
                    raise Exception("La ligne '",ligne,"' du fichier parametre n'est pas valide!")
        fh.close()
    except FileNotFoundError:
        raise Exception ("Le fichier n'existe pas.")
    validerParam(params)
    validerParamObligatoire(params)
    validerFichier(fichier_param)
    return params


def validerParam(parametres):
    for key in parametres.keys():
        if not key in (PARAMS_VALIDES + ALIAS_VALIDES+PARAMS_APP):
            raise Exception("Veuillez verifier vos parametres. Voir Guide PhyML Shell.")

def validerParamObligatoire (parametres):
    for element in parametres:
        if element:
            for i in PARAMS_OBLIGATOIRES:
                if not i in parametres.keys():
                    raise Exception("Tous les parametres obligatoires ne sont pas presents. Voir Guide PhyML Shell.")

