# -*- coding: utf-8 -*-
'''
Created on Jan 22, 2016

@author: lionnel
'''

from subprocess import Popen, DEVNULL, PIPE
from uuid import uuid4
from os.path import isfile, join, dirname
from os import remove, rmdir, listdir
from read_and_viewfns_2 import prep_working_directory,contient_fichier_vide,\
    view_helper
import ntpath
from phyml_util import *

def get_input_file(**param):
    for key in INPUTFILE_PARAM_LIST:
            if key in param: return param[key]

def create_supplement_dict_val(working_directory_path):
    d = {}
    d[RUN_ID] = str(uuid4())
    d[KEY_WORK_SUBDIRECTORY] = join(working_directory_path,d[RUN_ID])
    return d


def run_helper(run_object,**param):    
    input_file = get_input_file(**param)
    if(not isfile(input_file)) : 
        raise FileNotFoundError(); 
#     path_key, direc = KEY_SUBDIRECTORY_INPUT_FILEPATH, KEY_WORK_DIRECTORY
#     val = prep_working_directory(input_file,param[direc],param[RUN_ID])
#     run_object.add_param(path_key,val)
#     param[path_key] = val
    return  Popen(command_builder(**param),stderr=PIPE,stdout=DEVNULL)


def command_builder(**param):
    arguments = []
    #adds the path to phyml executable
    arguments.append(param[KEY_PROGRAM_PATH])
    
    for cur_param,val in param.items():
        if cur_param is KEY_SUBDIRECTORY_INPUT_FILEPATH:
            arguments.append("--input") 
            arguments.append(val) 
            print("val:",val)
            continue
        #if the current parameter is a special case continue
        if cur_param in ["input","i",KEY_PROGRAM_PATH, KEY_WORK_DIRECTORY] : continue
        if len(cur_param)>0 : 
            #if the current parameter stand on is own ie. does not have a second value 
            if cur_param in KEYS_WITH_NO_2ND_PARAMETERS_LIST : 
                if val in VALUES_TO_REJECT : continue
                if val in VALUES_TO_ACCEPT : arguments.append( "--"+cur_param)
                continue
            #normal parameters --parameter_name value
            elif cur_param in ALIAS_VALIDES : cur_param = "-"+cur_param
            elif cur_param in PARAMS_VALIDES : cur_param = "--"+cur_param
            else: continue
            arguments.append(cur_param)
            if len(val)>0 : arguments.append(val)
#     print("La commande creer est:\n",arguments)
    return arguments


def clean_up_folder(directory_path,input_path,user_def_id):
    input_filename = ntpath.basename(input_path)
    erase_input_file = True
    if(str(dirname(input_path)) is directory_path): erase_input_file = False
#     print(str(dirname(input_path)), directory_path)
    
    filelist = [f for f in listdir(path=directory_path) if f.find(input_filename)>=0 ]
    
    print(filelist)
    
    remove_dir = True
    for file in filelist:
        if file != input_filename or erase_input_file: 
            print("removing:",file)
            remove(join(directory_path,file))
        #if the current file is there original input file the directory must not be deleted
        else : remove_dir=False
    nb_file = len(listdir(directory_path) )
    print(nb_file," files in folder : ",directory_path)
    #if directory is empty remove folder named process_id
    if remove_dir and nb_file == 0: 
        print("removing:",directory_path)
        rmdir(directory_path)

def reset_helper(run_instance,directory_path,input_path):
    if run_instance is None:
        raise Exception(MSG_ERROR_NO_PROCESS)
    if run_instance.poll() is None : 
        run_instance.kill()
        clean_up_folder(directory_path,input_path,"")
    else: 
        raise Exception(MSG_PROCESS_ALREADY_STOPPED)

def status_helper(cur_process,cur_status,**param):
    directory = param[KEY_WORK_SUBDIRECTORY]
    user_run_id = param.get("run_id")
    if user_run_id is  None:
        user_run_id =""
    if cur_status > 1 :
        return cur_status
    if cur_process is None : 
        return PROCESS_NOT_STARTED
        
    returncode = cur_process.poll()
    if returncode is not None :
        if returncode>=0 : 
#             print(cur_process.communicate()) 
            created_file_list = view_helper(directory,get_input_file(**param),user_run_id)
#             print(directory,created_file_list)
            if(contient_fichier_vide(created_file_list) or len(created_file_list)<=1):
                return PROCESS_FINISHED_WITH_ERROR
            return PROCESS_FINISHED_NORMALLY
        #all negative values means that the process was terminated
        else: return PROCESS_TERMINATED
    return PROCESS_RUNNING

