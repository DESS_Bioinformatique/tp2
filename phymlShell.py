#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
@author    :    Elodie Hip-Ki #HIPE24608903
@author    :    Lionnel Guefack Lemogo #LEML12018206
@author    :    J.Eric Munganyiki #MUNJ09068401
@version   :    0.1
@summary   :    mini-Shell en Python offrant des commandes permettant l'utilisation  
du programme phyML. PhyML est un programme dont la tache principale est d’estimer le maximum de vraisemblance
de phylogenies a partir d’alignement de nucleotides ou d’acides amines.

Plus precisement, les commandes suivantes sont implementees :
    help       : Affiche la liste des commandes.
    run        : Lance l’exécution de l'objet PhyML.
    create     : Cree un objet RunPhyML avec les parametres fournis au constructeur.
    status     : retourne le status d’execution : (0) non commencee / (1) en cours / (2) terminee
                avec succes / (3) terminee avec un echec
    reset      : Permet d’interrompre une execution et d’en effacer toute les traces.
    view       : Affiche la liste des fichiers generes.
    read       : Affiche le contenu du fichier (s'il existe).
    exit / quit : Quitte le programme.


'''

from runPhyml import RunPhyml
import pickle
import os
from phyml_util import VALUES_TO_ACCEPT
from read_and_viewfns_2 import prep_working_directory

def fin_du_programme( liste_cmd ):
    """ Retourne True si la commande saisie est quit ou exit, sinon False """
    if (commande_est( "exit", liste_cmd) or commande_est( "quit", liste_cmd) ) :
        unfinished_run = False
        for inst_id, inst in phymlInstanceHashTableList.items():
            if inst.status() == 1:
                MSG_PROCESS_STILL_RUNNING = "Le processus ( " + str(inst_id) + " ) est en cours. Appuyez sur (y) pour l'arreter:"
                ans = input(MSG_PROCESS_STILL_RUNNING)
                if ans in VALUES_TO_ACCEPT:
                    inst.reset()
                else: unfinished_run =True
        if unfinished_run: print(MSG_EXIT_WITH_ZOMBIE_PROCESS)
        else : print(MSG_SORTIE_NORMAL)
        try :
            pickle.dump(phymlInstanceHashTableList, open(DATAFILE, mode='w+b'))
        except: print("La liste de processus n'a pas pu etre sauvegarde!")
        return True
    return False


def lire_commande():
    """ Retourne une liste de la commande + les options lus au clavier """
    return input("phymlShell> ").split()


def commande_est( cmd_a_tester, liste_cmd):
    """ Retourne True si la commande est tester est celle qui est saisie """
    if len(liste_cmd) == 0:
        return False
    return cmd_a_tester == liste_cmd[0]


def afficher_help():
    """ Affiche l'aide """
    print(MSG_AIDE)

    
def afficher_message_erreur():
    """ Affiche un message d'erreur pour une commande inconnue """
    print(MSG_COMMANDE_INCONNUE)


def create_runPhyml(liste_cmd):
    phymlInstance = None
    if len(liste_cmd) >1:
        try:
            phymlInstance = RunPhyml(liste_cmd[1])
            phymlInstanceHashTableList[len(phymlInstanceHashTableList)]=phymlInstance
        except Exception as e:
            print(e)
    else:
        print(MSG_FILE_MISSING)
    return phymlInstance

def reset_phyml(phymlInstance):
    if phymlInstance is None:
        print(MSG_CREATE_RUNPHYML_BEFORE)
    else:
        try :
            phymlInstance.reset()
        except Exception as e:
            print(e)


def get_phyml_status(phymlInstance):
    if phymlInstance is None:
        print(MSG_CREATE_RUNPHYML_BEFORE)
    else:
        print("status est :", phymlInstance.status())


def view_created_files(phymlInstance):
    file_list = []
    if phymlInstance is None:
        print(MSG_CREATE_RUNPHYML_BEFORE)
    else:
        try:
            file_list = phymlInstance.view()
            nb_file = len(file_list)
            if nb_file > 0:
                print("Phyml a creer les (", nb_file, ") fichier(s) suivant(s):")
                for file in file_list:
                    print(file)
            else :
                print(MSG_NO_FILE_CREATED)
        except FileNotFoundError as e:
            print(e)


def run_phyml_instance(phymlInstance):
    if phymlInstance is None:
        print(MSG_CREATE_RUNPHYML_BEFORE)
    elif phymlInstance.status() == 0:
        try:
            phymlInstance.run()
            print(MSG_RUN_STARTED,phymlInstance.work_directory)
        except FileNotFoundError as f:
            print(MSG_FILE_NOT_FOUND, f)
        except Exception as e:
            print(e)
    elif phymlInstance.status() == 1:
        print(MSG_IS_RUNNING)
    else:
        print(MSG_ALREADY_RUN)


def read_file(liste_cmd, phymlInstance):
#     RunPhyml.read(phymlInstance, liste_cmd[1])
    if not len(liste_cmd) >1 :
        print(MSG_FILE_MISSING)
    else:
        try:
            RunPhyml.read(phymlInstance, liste_cmd[1])
        except FileNotFoundError:
            print(MSG_FILE_NOT_FOUND)
        except:
            print(MSG_ERROR_READ_FILE, liste_cmd[1], ".")


def show_list(phymlInstanceHashTableList):
    if len(phymlInstanceHashTableList.items()) > 0:
        print("ID\tSTATUS\tDOSSIER DE TRAVAIL")
        for inst_id, inst in phymlInstanceHashTableList.items():
            print(inst_id, "\t", inst.status(), "\t", inst.work_directory)
    else:
        
        print(MSG_NO_INSTANCE)


#declaration des constantes ==========================================================
MSG_OUVERTURE_SESSION               =   "==============================================================================================\n"\
                                    "== phyMlShell version 0.1, par Elodie Hip-Ki, Lionnel Lemogo, J.Eric Munganyiki (Hiver 2016)==\n"\
                                    "==============================================================================================\n"
MSG_AIDE                            =   "== Liste des commandes du shell == \n"\
                                        "help\t\t: affiche la liste des commandes.\n"\
                                        "create\t\t: cree un objet RunPhyML avec les parametres fournis au constructeur.\n"\
                                        "run\t\t\t: lance l’execution de l'objet RunPhyML.\n"\
                                        "status\t\t: retourne le status d’execution : (0) non commencee / (1) en cours / (2) terminee avec succes / (3) terminee avec un echec\n"\
                                        "reset\t\t: permet d’interrompre une execution et d’en effacer toutes les traces.\n"\
                                        "view\t\t: affiche la liste des fichiers generes.\n"\
                                        "read\t\t: affiche le contenu d'un fichier (s'il existe).\n"\
                                        "exit/quit\t: quitte le programme.\n"\

MSG_SORTIE_NORMAL                   =   "Fin normale du shell, a bientot..."
MSG_COMMANDE_INCONNUE               =   "La commande saisie n'est pas reconnue. Veuillez verifier la "\
                                        "commande saisie."
MSG_CREATE_RUNPHYML_BEFORE          =   "Creez une instance de runPhyml avant."
MSG_NO_FILE_CREATED                 =   "Le programme ne semble pas avoir cree de fichier."
MSG_ALREADY_RUN                     =   "L'instance phyml a deja ete executee."
MSG_IS_RUNNING                      =   "L'instance phyml est deja en execution."
MSG_FILE_NOT_FOUND                  =   "Le fichier input n'a pas ete trouve."
MSG_ERROR_READ_FILE                 =   "Il y a eu une erreur lors de la lecture du fichier."
MSG_RUN_STARTED                     =   "L'execution de phyml a ete lancee. Les fichiers de l'execution se trouveront dans le dossier :"
MSG_EXIT_WITH_ZOMBIE_PROCESS        =   "Le programme se termine avec des processus zombie en cours d'execution!"
MSG_NO_INSTANCE                     =   "Aucune instance phyml n'a ete creee. Faites 'create chemin_fichier_parametre'."
DATAFILE = "phymlShell.dat"
MSG_FILE_MISSING                    =   "Le chemin vers le fichier est manquant."
MSG_UNEXPEXTED_ERROR                =   "Le programme viens de recuperer d'une erreur innatendue."

liste_cmd = []
phymlInstance = None

print(MSG_OUVERTURE_SESSION)

liste_cmd = lire_commande()

open(DATAFILE, mode='a').close()
if os.stat(DATAFILE).st_size == 0:
    phymlInstanceHashTableList = {}
else:
    phymlInstanceHashTableList=pickle.load(open(DATAFILE, mode='r+b'))
#print(phymlInstanceHashTableList)


while not fin_du_programme(liste_cmd):
#     try:
    process_id = "1"
    filepath = "/home/lionnel/workspace/INF8214_tp2/test.phy"
    directorypath = "/home/lionnel/tmp"
    subdirectory = prep_working_directory(filepath,directorypath,process_id)
    param ={
"rep" : directorypath
,"datatype" : "aa"
,"phyml" : "/usr/bin/phyml"
,"input" : filepath
,"id_repertory" : process_id
,"id" : process_id
,"subdirectory": subdirectory
            }
    phymlInstance = RunPhyml(param)
    phymlInstance.run()
#         if commande_est("create", liste_cmd):
#             phymlInstance = create_runPhyml(liste_cmd)
#         elif commande_est("run", liste_cmd):
#             run_phyml_instance(phymlInstance)
#         elif commande_est("reset", liste_cmd):
#             reset_phyml(phymlInstance)
#         elif commande_est("status", liste_cmd):
#             get_phyml_status(phymlInstance)
#         elif commande_est("view", liste_cmd):
#             view_created_files(phymlInstance)
#         elif commande_est("read", liste_cmd):
#             read_file(liste_cmd, phymlInstance)
#         elif commande_est("help", liste_cmd):
#             afficher_help()
#         elif commande_est("status0", liste_cmd):
#             if len(liste_cmd) > 1:
#                 tmp = phymlInstanceHashTableList[int(liste_cmd[1])]
#             else: tmp = phymlInstance
#             get_phyml_status(tmp)
#         elif commande_est("run0", liste_cmd):
#             if len(liste_cmd) > 1:
#                 tmp = phymlInstanceHashTableList[int(liste_cmd[1])]
#             else: tmp = phymlInstance
#             run_phyml_instance(tmp)
#         elif commande_est("reset0", liste_cmd):
#             if len(liste_cmd) > 1:
#                 tmp = phymlInstanceHashTableList[int(liste_cmd[1])]
#             else: tmp = phymlInstance
#             reset_phyml(tmp)
#         elif commande_est("view0", liste_cmd):
#             if len(liste_cmd) > 1:
#                 tmp = phymlInstanceHashTableList[int(liste_cmd[1])]
#             else: tmp = phymlInstance
#             view_created_files(tmp)
#         elif commande_est("list", liste_cmd):
#             show_list(phymlInstanceHashTableList)
#         
#         elif (len(liste_cmd)>0):
#             afficher_message_erreur()
#             afficher_help()
#     except:
#         print(MSG_UNEXPEXTED_ERROR)
    liste_cmd = lire_commande()

            
