# -*- coding: utf-8 -*-


#Liste des variables globales
PROCESS_NOT_STARTED = 0
PROCESS_RUNNING = 1
PROCESS_FINISHED_NORMALLY = 2
PROCESS_FINISHED_WITH_ERROR = 3
PROCESS_TERMINATED = 3
# PROCESS_TERMINATED = -9
KEY_WORK_DIRECTORY = "rep"
KEY_PROGRAM_PATH = "phyml"
PARAMS_VALIDES = ["rep", "input", "model", "datatype", "sequential", "bootstrap", "multiple", "phyml", "pars",\
                      "ts/tv", "pinv", "nclasses", "alpha", "use_median", "free_rates", "codpos", "aa_rate_file",\
                      "search", "inputtree", "rand_start", "n_rand_starts", "r_seed",\
                      "print_site_lnl", "print_trace", "run_id", "no_memory_check", "no_jcolalias", "contrained_lens",\
                      "constraint_file", "quiet", "alias_subpatt", "boot_progress_display","datatype"]
ALIAS_VALIDES = ["i", "d", "q", "n", "p", "b", "m", "f", "t", "v", "c", "a", "s", "u", "o"]
PARAMS_OBLIGATOIRES = ["rep", "phyml", "input","datatype"]
KEY_WORK_SUBDIRECTORY = "id_repertory"
KEY_SUBDIRECTORY_INPUT_FILEPATH = "subdirectory"
KEY_RUN_ID = 'id'
KEY_INPUT_FILENAME = 'input'
STATUS = 'status'
DATE ="date"
PARAMS_APP = [KEY_WORK_SUBDIRECTORY,KEY_SUBDIRECTORY_INPUT_FILEPATH,KEY_RUN_ID,KEY_PROGRAM_PATH,KEY_WORK_DIRECTORY,STATUS,DATE]
INPUTFILE_PARAM_LIST = ["input", "i"]
KEYS_WITH_NO_2ND_PARAMETERS_LIST = ["p","pars","q","sequential","quiet","contrained_lens","no_jcolalias","no_memory_check",\
                                    "print_trace","print_site_lnl","rand_start","free_rates","use_median"]
# FILE_EXTENTION_LIST = ["tree.txt","stats.txt","boot_trees.txt","boot_stats.txt","rand_trees.txt",\
#                        "phyml_lk.txt"]
FILE_EXTENTION_LIST = ["tree","stats","boot_trees","boot_stats","rand_trees","phyml_lk"]
VALUES_TO_REJECT = ["no","off"]
VALUES_TO_ACCEPT = ["yes","Yes","YES","on","ON","On"]
MSG_ERROR_NO_PROCESS = "Erreur! Il n'y a aucun processus a arreter."
MSG_PROCESS_ALREADY_STOPPED = "Le processus est deja arrete ou il n'y a aucun processus a arreter."
