#!/usr/bin/python

from flask import Flask, render_template, request, jsonify
import bd_phyml
import os
from runPhyml import RunPhyml
from lireParam import validerParam, validerParamObligatoire
from phyml_util import *
from read_and_viewfns_2 import readhelper
import datetime
import configparser
import ast

# Setup Flask app.
app_phyml = Flask(__name__)
app_phyml.debug = True
bd_phyml.init_db()
runphymlInstances = {}
config = configparser.ConfigParser()
config.read("phymlWeb.ini");
DIRECTORYPATH = config["DEFAULT"]["directorypath"]
PHYML_PATH = config["DEFAULT"]["phyml"]

@app_phyml.route ('/delete', methods=['GET','POST'])
def supprimerRun ():
    process_id=request.form["id"]
    print("id:",(process_id))
    bd_phyml.delete((request.form["id"]))
    if(runphymlInstances.get(process_id)is not None):
        r = runphymlInstances.pop(process_id)
        try : r.reset()
        except: 
                r.clean_up()
    return default()

@app_phyml.route ('/run', methods=['POST'])
def lancerRun():
    cur_id = request.json["id"]
    r = runphymlInstances[cur_id]
    r.run()
    print("status:",r.status)
#TO DO:envoyer message d'erreur
    return jsonify({"id":cur_id,"status":r.status})
    
@app_phyml.route ('/new', methods=['POST'])
def ajouterRun ():
    if request.json is not None:
        form = {}
        json = request.json
        print("json:",request.json)
        form[DATE] = str(datetime.datetime.now())
        values = json["values"]
        for v in values:
            if  v["name"] in PARAMS_VALIDES:
                form[v["name"]] = v["value"]
        file=str(json['inputfile'])
        filename = json['filename']
        form['datatype']=json['datatype']
    try:
        tmp = get_parameters(form,"-1", "bidon")
        validerParam(tmp)
        validerParamObligatoire(tmp)
    except:
        print("Il y a eu une erreur lors de la validation.")
    process_id = str(bd_phyml.add( ("0", filename, "{}" ) ))
    new_subdirectory, filepath = prep_workdirectory(file, filename, process_id)
    params = get_parameters(form,process_id, new_subdirectory, filepath,filename)
    bd_phyml.setParams((str(params),str(process_id)))
    r = RunPhyml(params, bd_phyml.setStatus)
    #r.run()
    runphymlInstances[process_id] = r
    print('after bd update',process_id,r)
    return jsonify({"id":process_id,"status":0})

@app_phyml.route ('/read', methods=['POST'])
def lireFichier ():
    json = request.json
    filepath = json["filepath"]
    return jsonify({"filepath":filepath,"content":readhelper(os.path.join(DIRECTORYPATH,filepath))})

@app_phyml.route ('/statusx', methods=['POST'])
def afficherStatut ():
    cur_id = request.json["id"]
    cur_status = str(runphymlInstances[cur_id].status)
    print({"id":cur_id,"status":cur_status})
    return jsonify({"id":cur_id,"status":cur_status})

@app_phyml.route ('/view', methods=['POST'])
def afficherListeFichiers ():
    ids = {}
    inputfiles = {}
    execution = {}
    outputfiles = {}
    stat = {}
    for i in runphymlInstances:
        r=runphymlInstances[i]
        ids[i]=r.runid
        inputfiles[i]=r.inputfile
        outputfiles[i]=r.view()
        execution[i]=r.date
        stat[i]=r.status

    result = {"id": ids, "inputfile":inputfiles,"execution":execution,"outputfile":outputfiles, "status":stat}
    return jsonify(result)

@app_phyml.route ('/reset', methods=['POST'])
def resetRun ():
    cur_id = request.json["id"]
    try :
        r = runphymlInstances[cur_id]
        r.clean_up()
        r.set_status(0)
        r.setProcess(None)
        bd_phyml.setStatus(("0",cur_id))
        cur_status = str(bd_phyml.getStatus((cur_id)))
        return jsonify({"id":cur_id,"status":cur_status})
    except Exception as e:
        print({"error":str(cur_id)+str(e)})
        return jsonify({"error":str(cur_id)+str(e)})

@app_phyml.route('/')
def default():
    records = bd_phyml.get()
    return render_template('index.html', runs=records)

@app_phyml.route('/<path:path>')
def all_files(path):
    return app_phyml.send_static_file(path)

@app_phyml.errorhandler(404)
def pas_trouver(error=None):
    mes = {
            'status': 404,
            'message': 'Non trouve: ' + request.url,
    }
    resp = jsonify(mes)
    resp.status_code = 404
    return resp

#############################################################
##util functions
#############################################################
def get_parameters(form, process_id, new_subdirectory, filepath,filename=""):
    d = form
    d[KEY_INPUT_FILENAME] = os.path.join(process_id, filename) #input
    d[KEY_RUN_ID] = process_id
    d[KEY_WORK_DIRECTORY] = DIRECTORYPATH
    d[KEY_PROGRAM_PATH] = PHYML_PATH
    d[KEY_WORK_SUBDIRECTORY] = new_subdirectory
    d[KEY_SUBDIRECTORY_INPUT_FILEPATH] = filepath
    d[STATUS] = 0
    return d


def prep_workdirectory(file, filename, process_id):
    new_subdirectory = os.path.join(DIRECTORYPATH, process_id)
    if (not os.path.isdir(new_subdirectory)):
        os.makedirs(new_subdirectory) #copy file to new subdirectory
    filepath = os.path.join(new_subdirectory, filename)
    f = open(filepath,"w")
    f.write(file)
    return new_subdirectory, filepath

def load_db_data():
    records = bd_phyml.get();
    for row in records:
        process_id = str(row[0])
        new_subdirectory = os.path.join(DIRECTORYPATH, process_id)
        filename = row[2]
        cur_status = bd_phyml.getStatus((process_id))
        filepath = os.path.join(new_subdirectory, filename)
        param= ast.literal_eval(row[3])
        r = RunPhyml(get_parameters(param,process_id, new_subdirectory, filepath,filename), bd_phyml.setStatus)
        r.set_status(int(cur_status))
        runphymlInstances[process_id]=r
    for r in runphymlInstances:
        print(r);

if __name__ == '__main__':
    bd_phyml.init_db()
    load_db_data();
    app_phyml.run(debug=True)
