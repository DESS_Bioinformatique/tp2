# -*- coding: utf-8 -*-
from lireParam import *
from read_and_viewfns_2 import readhelper, view_helper
from phyml_util import *
from subprocess import Popen, DEVNULL, PIPE
from os.path import isfile, join, dirname
from os import remove, rmdir, listdir
from read_and_viewfns_2 import prep_working_directory,contient_fichier_vide,\
    view_helper
import ntpath
import threading

class RunPhyml:
    counter = 0
    def __init__(self,dicParam, callback):
        self.__param = dicParam
        validerParam(dicParam)
        validerParamObligatoire(dicParam)
        print("input file path",self.__param[KEY_SUBDIRECTORY_INPUT_FILEPATH])
        if(not isfile(self.__param[KEY_SUBDIRECTORY_INPUT_FILEPATH])) : 
            raise Exception("Le fichier input n'a pas ete trouvee");
        self.__process = None
        self.__cur_status = 0
        self.__callback = callback


    def run(self):
        #the execution is started in a thread to attach to it a callback function to update the database
        thread = threading.Thread(target=self.runInThread, args=(self.__callback,self,self.__param))
        thread.start()
#         thread.

    def runInThread(self, onExit, instance, popenArgs):
#         self.__process = self.run_helper(self,**popenArgs)
        instance.setProcess( self.run_helper(self,**popenArgs) )
        print("process:",instance.process)
        instance.process.wait()
        print("just completed running thread",self.__param)
        print("status",onExit((self.status,self.runid)))
        return

    @property
    def process(self):
        return self.__process
    
    def setProcess(self,newProcess):
        self.__process = newProcess

    @property
    def runid(self):
        return self.__param[KEY_RUN_ID]

    @property
    def date(self):
        return self.__param[DATE]

    @property
    def inputfile(self):
        return self.__param["input"]

    @property
    def work_directory(self):
        return self.__param[KEY_WORK_SUBDIRECTORY]

    @property
    def status(self):
        """
        0: non commencee
        1: en cours
        2: terminee avec succes
        3: terminee avec echec
        """
        self.__cur_status = self.status_helper(self.__process,self.__cur_status,**self.__param)
        return self.__cur_status
    
    def status_helper(self, cur_process,cur_status,**param):
        if cur_status == 2 :
            return cur_status
        if cur_process is None : 
            return PROCESS_NOT_STARTED
        resultDirectory = param[KEY_WORK_DIRECTORY]
        returncode = cur_process.poll()
        if returncode is not None :
            if returncode>=0 : 
                directory = param[KEY_WORK_SUBDIRECTORY]
                user_run_id = param.get("run_id")
                if user_run_id is  None:
                    user_run_id =""
                created_file_list = view_helper(directory,self.get_input_file(**param),self.runid,user_run_id)
                print("in status helper-- file list:",created_file_list)
                if(contient_fichier_vide(created_file_list,resultDirectory)):
#                 if(contient_fichier_vide(created_file_list,resultDirectory) or len(created_file_list)<=1):
                    return PROCESS_FINISHED_WITH_ERROR
                return PROCESS_FINISHED_NORMALLY
            #all negative values means that the process was terminated
            else: return PROCESS_TERMINATED
        return PROCESS_RUNNING

    def reset(self):
        #interompt une execution et en efface les traces
        self.reset_helper(self.__process,self.__param[KEY_WORK_SUBDIRECTORY],self.get_input_file(**self.__param))
        self.__process = None

    def view(self):
        #retourne la liste des fichiers generes
        user_runid = self.__param.get("run_id")
        if user_runid is  None : user_runid = ""
        direc,input_path = self.__param[KEY_WORK_SUBDIRECTORY],self.inputfile
        return view_helper(direc,input_path,self.runid,user_runid)
    
    def add_param(self, key, val):
        print("add_param",key,val)
        self.__param[key] = val
        return self.__param
    
    def set_status(self, val):
        self.__cur_status=val

    def read(self,file):
        #retourne le contenu du fichier (s'il existe)
        return readhelper(file)

    def __str__(self):
        return "La commande de l'instance ("+str(self.runid)+") est :"+str(self.command_builder(**self.__param))
    
    def get_input_file(self, **param):
        for key in INPUTFILE_PARAM_LIST:
                if key in param: return param[key]
    
    
    def run_helper(self, run_object,**param):    
        input_file = self.__param[KEY_SUBDIRECTORY_INPUT_FILEPATH]
        if(not isfile(input_file)) : 
            raise FileNotFoundError(); 
        return  Popen(self.command_builder(**param),stderr=PIPE,stdout=DEVNULL)
        
    
    def command_builder(self, **param):
        arguments = []
        param = self.__param
        #adds the path to phyml executable
        arguments.append(param[KEY_PROGRAM_PATH])
        
        for cur_param,val in param.items():
            if cur_param == KEY_SUBDIRECTORY_INPUT_FILEPATH:
                arguments.append("--input") 
                arguments.append(val) 
                print("val:",val)
                continue
            #if the current parameter is a special case continue
            if cur_param in ["input","i",KEY_PROGRAM_PATH, KEY_WORK_DIRECTORY] : continue
            if len(cur_param)>0 : 
                #if the current parameter stand on is own ie. does not have a second value 
                if cur_param in KEYS_WITH_NO_2ND_PARAMETERS_LIST : 
                    if val in VALUES_TO_REJECT : continue
                    if val in VALUES_TO_ACCEPT : arguments.append( "--"+cur_param)
                    continue
                #normal parameters --parameter_name value
                elif len(str(val))==0:continue
                elif cur_param in ALIAS_VALIDES : cur_param = "-"+cur_param
                elif cur_param in PARAMS_VALIDES : cur_param = "--"+cur_param
                else: continue
                arguments.append(cur_param)
                if len(val)>0 : arguments.append(val)
        print("La commande creer est:\n",arguments)
        return arguments
    
    def clean_up(self):
        directory_path = self.__param[KEY_WORK_SUBDIRECTORY] 
        self.clean_up_folder(directory_path, self.__param[KEY_SUBDIRECTORY_INPUT_FILEPATH])
        self.__cur_status = 0
        if self.__process is not None:
#             raise Exception(MSG_ERROR_NO_PROCESS)
            if self.__process.poll() is None : 
                self.__process.kill()
                self.clean_up_folder(directory_path, self.__param[KEY_SUBDIRECTORY_INPUT_FILEPATH])
                self.__process = None
#         else: 
#             raise Exception(MSG_PROCESS_ALREADY_STOPPED)
    
    
    def clean_up_folder(self, directory_path,input_path):
        input_filename = ntpath.basename(input_path)
        erase_input_file = True
        if(str(dirname(input_path)) == directory_path): erase_input_file = False
        filelist = [f for f in listdir(path=directory_path) if f.find(input_filename)>=0 ]
        print("\n\ninput_filename:",input_filename,"erase_input_file:",erase_input_file)
        print("directorypath:",directory_path,"using_dirname:",dirname(input_path))
        print("clean_up_folder filelist:",filelist)
        remove_dir = True
        for file in filelist:
            if file != input_filename or erase_input_file: 
                print("removing:",file)
                remove(join(directory_path,file))
            #if the current file is there original input file the directory must not be deleted
            else : remove_dir=False
        nb_file = len(listdir(directory_path) )
        print(nb_file," files in folder : ",directory_path)
        #if directory is empty remove folder named process_id
        if remove_dir and nb_file == 0: 
            print("removing:",directory_path)
            rmdir(directory_path)
    
    def reset_helper(self, run_instance,directory_path,input_path):
        if run_instance is None:
            raise Exception(MSG_ERROR_NO_PROCESS)
        if run_instance.poll() is None : 
            run_instance.kill()
            self.clean_up_folder(directory_path,input_path)
        else: 
            raise Exception(MSG_PROCESS_ALREADY_STOPPED)
