$(document).ready( function () {

	var reader = new FileReader();
	reader.onload = function(e) {
		if (valider()){
			var text = reader.result;
			envoi_form([text,data.name]);
			addToTable();
		}
		else {
	$("#result").html('<div class="alert alert-danger alert-dismissible" role="alert">'+
						'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
						'<span aria-hidden="true">&times;</span></button>Veuillez vérifier vos paramètres.</div>');
		};
	};

//	== CONTROLEUR ==
	$("#create").click(function(){
		input=document.getElementById('inputfile').files;
		if (input.length==0){
			alert("Le champ input ne peut pas etre vide.");
			return false;
		};
		data = document.getElementById('inputfile').files[0];
		text = reader.readAsText(data);
		return false;
	});

	//== FONCTION ==
	function envoi_form (input){
		values = $( "form" ).serializeArray();
		json = {"values": values, "inputfile":input[0],"filename":input[1],"datatype":$("#datatype").val()};
		console.log(json);
		$.ajax({
			type: 'POST',
			contentType: 'application/json',
			data: JSON.stringify(json),
			dataType: 'json',
			url: '/new'
		}).success(function(data){
			console.log(data);
//			result = jQuery.parseJSON(data);
			$("#result").html('<div class="alert alert-success alert-dismissible" role="alert">'+
					'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
					'<span aria-hidden="true">&times;</span></button>Instance créée: id: '+data.id+'\t status: '+data.status+'</div>');
		}).error(function(){
			$("#result").html('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Erreur du côté serveur</div>')
		}).done(function(){
			console.log("done");
		});
	}
	
	var refreshCounter =0;
	addToTable();
	var refreshTable = setInterval(function(){
		addToTable() // this will run after every 5 seconds
		console.log("auto refresh table"+refreshCounter++)
	}, 5000);

	function addToTable(){
		json ={}
		$.ajax({
			type: 'POST',
			contentType: 'application/json',
			data: JSON.stringify(json),
			dataType: 'json',
			url: '/view'
		}).success(function(data){
			console.log(data);
			var trHTML ="";
			var tabData = [];
			$.each(data.id, function (i,item) {
				outputs="";
				if (data.status[i]==1){
					console.log(data.status[i])
					outputs += '<div><img src="ajax-loader2.gif"></img></div>'
				}

				$.each(data.outputfile[i],function(j,curFile){

					outputs+='<a href="#display" onclick="getFile(\''+curFile+'\')">'+ curFile + '</a>';
				});
				buttons = 				'<div class="btn-group" role="group" aria-label="...">'+
				'<button type="button" id="run-'+item+'" class="btn btn-default">Run</button>'+
				'<button type="button" id="reset-'+item+'" class="btn btn-default ">Reset</button>'+
				'<button type="button" id="status-'+item+'" class="btn btn-default">Status</button></div>';
				inputFile = '<a href="#display" onclick="getFile(\''+data.inputfile[i]+'\')">'+ data.inputfile[i] + '</a>';

				trHTML += '<tr><td id="item">' + item + '</td>'+
				'<td>'+inputFile+'</td>'+
				'<td>' + data.execution[i] + '</td>'+
				'<td id="logo">' + outputs +'</td><td>'
				+ buttons
				+'</td></tr>';

				tabData.push( [item,inputFile,data.execution[i],outputs,buttons]);
			});
			 

			$("#tableBase").DataTable().clear();

			$('#tableBase').DataTable().rows.add(tabData).draw();

		}).error(function(){
			$("#result").html("<font color=red>Erreur du côté serveur</font>")
		}).done(function(){
			console.log("done");
		});
	};


	$("#table").on('click',"button[id^=status-]", function(){
		id = this.id.split("-")[1];
		console.log(id,this);
		getStatus({"id":id});
	});

	window.getStatus=function getStatus(json){
		$.ajax({
			url: "/statusx",
			type: "POST",
			data: JSON.stringify(json),
			contentType: "application/json",
			dataType: "json",
			success: function(data){
				console.log(data)
				console.log(json)
				$("#result").html('<div class="alert alert-success alert-dismissible" role="alert">'+
						'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
						'<span aria-hidden="true">&times;</span></button>id: '+data.id+'\t status: '+data.status+'</div>');
			},	
			error: function(){
				console.log("error");
			}
		});
	}

	$("#table").on('click',"button[id^=reset-]", function(){
		id = this.id.split("-")[1];
		console.log(id,this);
		doReset({"id":id});
	});

	window.doReset=function doReset(json){
		$.ajax({
			url: "/reset",
			type: "POST",
			data: JSON.stringify(json),
			contentType: "application/json",
			dataType: "json",
			success: function(data){
				console.log(data)
				console.log(json)
			},
			error: function(){
				console.log("error");
			}
		});
	}

	$("#table").on('click',"button[id^=run-]", function(){
		id = this.id.split("-")[1];
		console.log(id,this);
		doRun({"id":id});
	});

	window.doRun=function doRun(json){
		$.ajax({
			url: "/run",
			type: "POST",
			data: JSON.stringify(json),
			contentType: "application/json",
			dataType: "json",
			success: function(data){
				console.log(data)
			},
			error: function(){
				console.log("error");
			}
		});
	}

	window.getFile=function getFile(file){
		json ={}
		console.log(file);
		
		$.ajax({
			url: "/read",
			type: "POST",
			data: JSON.stringify({"filepath":file}),
			contentType: "application/json",
			dataType: "json",
			success: function(data){
			console.log(data);
			trHTML ='<div class="panel panel-success">'+
			'<div class="panel-heading">'+data.filepath+'</div>'+
			'<textarea class="panel-body" rows="20">'+
			data.content+
			'</textarea>'+
			'</div>';
			$('#display').html('<div class="alert alert-success alert-dismissible" role="alert">'+
						'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
						'<span aria-hidden="true">&times;</span></button>'+trHTML+'</div>');
		},error:function(){
			$("#result").html('<div class="alert alert-success alert-dismissible" role="alert">'+
						'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
						'<span aria-hidden="true">&times;</span></button><font color=red>Erreur du côté serveur</font></div>');
		}
	});
	}

	$("#otherOptions").hide();	
	$("#otherOptions").prop('disabled',true);	
	$("#showOtherOptions").click( function (){
		$("#otherOptions").toggle();
	});

	$('.aa_model').hide();//initialized with nt
	$('.aa_model').prop('disabled',true);
	$('#datatype').change(function() {
		if ($(this).val() == "nt") {
			$('.aa_model').hide();
			$('.aa_model').prop('disabled',true);
			$('.nucleotide_model').prop('disabled',false);
			$('.nucleotide_model').show();
		}else{
			$('.nucleotide_model').hide();
			$('.nucleotide_model').prop('disabled',true);
			$('.aa_model').show();
			$('.aa_model').prop('disabled',false);
		}
	});

	$('#nucleotide_model').change(function() {
		if  ($(this).val() == "custom_model_nt") {
			$('#custom_nt').show();
			$('#custom_aa').prop('disabled',true);
			$('#custom_nt').prop('disabled',false);
			$('#custom_aa').hide();
		}else{
			$('#custom_nt').hide();
			$('#custom_nt').prop('disabled',true);
			$('#custom_aa').prop('disabled',false);
		}
	});
	$('#aa_model').change(function() {
		if ($(this).val() == "custom_model_aa"){
			$('#custom_aa').show();
			$('#custom_nt').hide();
			$('#custom_aa').prop('disabled',false);
		}else{
			$('#custom_aa').hide();
			$('#custom_aa').prop('disabled',true);
		}
	});
	$('#datatype').change(function() {
		if ($(this).val() == "aa") {
			$('#tstv').hide();
			$('#tstv').prop('disabled',false);
		}else{
			$('#tstv').show();
			$('#tstv').prop('disabled',true);
		}
	});
	$('#search').change(function() {
		if ($(this).val() == "spr") {
			$('#rand_start').show();
			$('#n_rand_starts').show();
		}else{
			$('#rand_start').hide();
			$('#n_rand_starts').hide();
		}
	});
	$('#inputtree').change(function() {
		if($("#inputtree").val() == ''){
 			$('#contrained_lens').hide();
			$('#constraint_file').hide();
		}else{
			$('#contrained_lens').show();
			$('#constraint_file').show();
		}
	});

	$('#fn_options').hide();
	$('#f').change(function() {
		if($("#f").val() == 'fN'){
			$('#fn_options').show();
		}else{
			$('#fn_options').hide();
		}
	});

	function valider() {
		nb_data_sets = document.getElementById("multiple").value;
		bootstrap = document.getElementById("bootstrap").value;
		tstv = document.getElementById("tstv").value;
		pinv = document.getElementById("pinv").value;
		nclasses = document.getElementById("nclasses").value;
		fA = document.getElementById("fA").value;
		fC = document.getElementById("fC").value;
		fG = document.getElementById("fG").value;
		fT = document.getElementById("fT").value;
		alpha = document.getElementById("alpha").value;
		n_rand_starts = document.getElementById("n_rand_starts").value;
		r_seed = document.getElementById("r_seed").value;
		if((nb_data_sets =="" || nb_data_sets >= 0)
		&&(bootstrap == "" || bootstrap >=0 || bootstrap == -1 || bootstrap == -2 || bootstrap == -4)
		&&(tstv == "" || tstv >=0 || tstv=="e")
		&&(pinv == "" || pinv>=0 && pinv<=1 || pinv=="e")
		&&(nclasses == "" || nclasses > 0)
		&&(fA == "" || 0<=fA<= 1)
		&&(fC == "" || 0<=fC<= 1)
		&&(fG == "" || 0<=fG<= 1)
		&&(fT == "" || 0<=fT<= 1)
		&&(alpha == "" || alpha>=0 || alpha=="e")
		&&(n_rand_starts == "" || n_rand_starts >=0)
		&&(r_seed == "" || r_seed >=0)
		){
		return true;
		};
};

	$("#tableBase").dataTable();

});//end document ready