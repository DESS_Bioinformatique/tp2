##**Guide d'utilisation de PhyML Shell**  

 **1.Settings**  

S'assurer que les dépendances suivantes sont installées:  
	-Flask  
	-SQLite3  
	-os  
	-dépendances Python:  
	
 - datetime  
 - configparser  
 - ast  
 - werkzeug.utils    
 
S'assurer que les fichiers sont dans les bons répertoires.  
 >**./**  
 bd_phyml.py     lireParam.py                phymlShell.py   phymlWeb.ini       read_and_viewfns_2.py       
 phyml_util.py   	    process_helper.py    static  
app_phyml.py      formulaire_parametres.html  phyml.db             runPhyml.py     templates  
**./static:**  
ajax-loader2.gif css  fonts  js  results  
**./static/css:**  
bootstrap.css      bootstrap.min.css.map    bootstrap-theme.min.css                      ie10-viewport-bug-workaround.css  
bootstrap.css.map  bootstrap-theme.css      bootstrap-theme.min.css.map                  navbar-fixed-top.css  
bootstrap.min.css  bootstrap-theme.css.map  Fixed Top Navbar Example for Bootstrap.html  datatables.min.css
**./static/fonts:**  
glyphicons-halflings-regular.eot  glyphicons-halflings-regular.ttf   glyphicons-halflings-regular.woff2  
glyphicons-halflings-regular.svg  glyphicons-halflings-regular.woff  
**./static/js:**  
bootstrap.js  bootstrap.min.js  creation.js  ie10-viewport-bug-workaround.js  ie-emulation-modes-warning.js    jquery-2.2.3.min.js  npm.js 
dataTables.bootstrap.min.js  html5shiv.min.js  
**./static/results:**  
**./templates:**  
index.html  

S'assurer que le fureteur web supporte HTML5.  

 **2.phymlWeb.ini**  
 
Modifier les paramètres du fichier phymlWeb.ini  
	
 -  "directorypath" correspond au chemin du répertoire dans lequel se trouveront les fichiers de sortie générés par PhyML.  
 - "phyml" correspond au chemin vers PhyML (la version binaire correspondante à votre système d'exploitation).  

>Exemple de fichier phymlWeb.ini  
>![enter image description here](https://lh3.googleusercontent.com/-_t65SlOnl7E/VyKem2H0B1I/AAAAAAAAMAM/GJevmGTm3b0_xl7k-s8hG3W3gZfCgCmgQCLcB/s0/Capture+d%25E2%2580%2599e%25CC%2581cran+2016-04-28+a%25CC%2580+19.25.31.png "Capture d’écran 2016-04-28 à 19.25.31.png")



> Written with [StackEdit](https://stackedit.io/).